import { Component, OnInit } from '@angular/core';
import { CommonService } from '../Services/common.service';
import { ServerHttpService } from '../Services/server-http.service';
import * as _ from 'lodash';
import * as moment from 'moment';
@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss'],
})
export class AboutComponent implements OnInit {
  public title = 'Covid19';
  public globalData: any = [];
  public countriesData: any = [];
  constructor(private serverHttp: ServerHttpService) {}
  ngOnInit(): void {
    this.serverHttp.getSummary().subscribe((data) => {
      console.log(data);
      this.globalData = data.Global;
      this.countriesData = data.Countries;
    });
  }

  public orderBy(key: any, dir: any) {
    this.countriesData = _.orderBy(this.countriesData, key, dir);
  }
}
